import CartContainer from '../Cart/CartContainer';
import NavBar from './NavBar';
import styles from './style.modules.css';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getCartItems,
  calculateTotals,
} from '../../features/cart/cartSlice';

export const App = () => {
  const dispatch = useDispatch();
  const { cartItems } = useSelector(store => store.cart);

  useEffect(() => {
    dispatch(calculateTotals());
  }, [cartItems]);

  useEffect(() => {
    dispatch(getCartItems('random'));
  }, []);
  return (
    <h2 className={styles.main}>
      <NavBar />
      <CartContainer />
    </h2>
  );
};
